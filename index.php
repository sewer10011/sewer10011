<?php
	ini_set ("session.use_trans_sid", true);
	session_start(); 
	include ('lib/connect.php');
	include ('lib/fun_global.php');
//	del_coocie_session();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel='stylesheet' href='style/style.css' type='text/css' />
	<link rel='stylesheet' href='style/awesome/css/font-awesome.min.css' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Dosis:300' rel='stylesheet' type='text/css'>
	<link rel='stylesheet' href='style/modal_windows.css' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<!--<link rel='stylesheet' href='style/bootstrap/css/bootstrap.css' type='text/css' />-->
	<!--<link rel='stylesheet' href='style/bootstrap/css/bootstrap-theme.css' type='text/css' />-->
	<script type="text/javascript" src="script/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="script/jquery.js"></script>
	<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="script/ajaxupload.js"></script>
	<script type="text/javascript" src="script/moi_function.js"></script>
	<!--<script type="text/javascript" src="style/bootstrap/js/bootstrap.js"></script>-->
</head>
<body>
	<div class="all_page">
		<div class="header">
			<?php
			
				headers();
			?>
		</div>
		
		
		<div class="content_page">	
			<?php
				contents();
			?>
		</div>	
		<div class="footer">
			<?php
				footer();
			?>
		</div>
	</div>	
	
</body>
</html>