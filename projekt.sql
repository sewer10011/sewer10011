/*
SQLyog Community v11.51 (32 bit)
MySQL - 5.5.25 : Database - prodjekt
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`prodjekt` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `prodjekt`;

/*Table structure for table `magazin` */

DROP TABLE IF EXISTS `magazin`;

CREATE TABLE `magazin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_magazin` varchar(255) DEFAULT NULL,
  `url_magazin` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/*Data for the table `magazin` */

insert  into `magazin`(`id`,`name_magazin`,`url_magazin`) values (11,'Брючный','www.bruki.com'),(12,'Магазин_платей','www.platia.com'),(13,'Магазин_курточек','www.kurtozki.com'),(14,'Обувной','www.tapki.net');

/*Table structure for table `my_album` */

DROP TABLE IF EXISTS `my_album`;

CREATE TABLE `my_album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(255) DEFAULT NULL,
  `id_user` varchar(255) DEFAULT NULL,
  `name_content` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

/*Data for the table `my_album` */

insert  into `my_album`(`id`,`parent_id`,`id_user`,`name_content`) values (10,'0','86','проверочный'),(11,'0','86','второй проверочный'),(12,'0','86','третий проверочный'),(13,'0','85','альбом люгера'),(14,'0','85','альбом люгера 2'),(27,'0','85','альбом люгера 3'),(67,'27','85','39'),(71,'10','86','27'),(72,'10','86','45'),(73,'11','86','38'),(74,'11','86','25'),(75,'12','86','27'),(76,'12','86','45'),(82,'0','86','четвертый'),(83,'10','86','28'),(84,'0','87','первый альбом пети'),(85,'84','87','30'),(86,'84','87','43');

/*Table structure for table `my_veszi` */

DROP TABLE IF EXISTS `my_veszi`;

CREATE TABLE `my_veszi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(11) DEFAULT NULL,
  `id_tovar` varchar(11) DEFAULT NULL,
  `date_record` varchar(250) DEFAULT NULL,
  `id_magazin` varchar(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

/*Data for the table `my_veszi` */

insert  into `my_veszi`(`id`,`id_user`,`id_tovar`,`date_record`,`id_magazin`) values (26,'85','39','Tue, 28 Apr 2015 16:38:05 +0300','14'),(29,'86','27','Tue, 28 Apr 2015 16:51:25 +0300','13'),(30,'86','45','Tue, 28 Apr 2015 16:51:30 +0300','13'),(31,'86','28','Tue, 28 Apr 2015 16:51:34 +0300','13'),(32,'86','42','Tue, 28 Apr 2015 16:51:38 +0300','14'),(33,'86','38','Tue, 28 Apr 2015 16:51:46 +0300','14'),(34,'86','25','Tue, 28 Apr 2015 16:51:52 +0300','12'),(35,'87','24','Tue, 28 Apr 2015 16:52:29 +0300','12'),(36,'87','30','Tue, 28 Apr 2015 16:52:33 +0300','13'),(37,'87','43','Tue, 28 Apr 2015 16:52:40 +0300','14'),(41,'88','26','Wed, 29 Apr 2015 18:48:36 +0300','12'),(42,'88','47','Wed, 29 Apr 2015 18:48:43 +0300','11'),(43,'87','25','Mon, 04 May 2015 20:59:32 +0300','12'),(45,'87','39','Mon, 04 May 2015 22:52:51 +0300','14'),(46,'87','28','Mon, 04 May 2015 22:53:09 +0300','13'),(47,'87','38','Mon, 04 May 2015 22:55:06 +0300','14'),(49,'85','29','Tue, 12 May 2015 18:38:42 +0300','13'),(52,'85','24','Thu, 14 May 2015 11:52:28 +0300','12'),(53,'85','23','Thu, 14 May 2015 11:53:00 +0300','11');

/*Table structure for table `podpiski` */

DROP TABLE IF EXISTS `podpiski`;

CREATE TABLE `podpiski` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user_podpiszik` varchar(11) DEFAULT NULL,
  `id_user_podpisnoy` varchar(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/*Data for the table `podpiski` */

insert  into `podpiski`(`id`,`id_user_podpiszik`,`id_user_podpisnoy`) values (7,'88','86'),(13,'88','85'),(14,'87','86'),(15,'87','88'),(27,'85','88'),(33,'86','88');

/*Table structure for table `push_like` */

DROP TABLE IF EXISTS `push_like`;

CREATE TABLE `push_like` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cont` varchar(55) DEFAULT NULL,
  `id_user` varchar(55) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `push_like` */

insert  into `push_like`(`id`,`id_cont`,`id_user`) values (1,'18','85'),(2,'19','85'),(3,'20','85'),(4,'18','86'),(5,'17','85'),(6,'21','86'),(7,'19','86');

/*Table structure for table `razdel` */

DROP TABLE IF EXISTS `razdel`;

CREATE TABLE `razdel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_razdel` varchar(255) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `razdel` */

insert  into `razdel`(`id`,`name_razdel`) values (1,'odezda'),(2,'obuv'),(3,'golovnie_ubori'),(4,'slapi');

/*Table structure for table `statei` */

DROP TABLE IF EXISTS `statei`;

CREATE TABLE `statei` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_statei` varchar(255) DEFAULT NULL,
  `body_statei` varchar(20000) DEFAULT NULL,
  `sex` varchar(11) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/*Data for the table `statei` */

insert  into `statei`(`id`,`name_statei`,`body_statei`,`sex`) values (17,'статья про тапки','<p>статья про тапки ууууууллллллааааалллллааалла!!!!</p>\r\n','man'),(18,'башмаки','<p>Башмаки бабские!!!!!!!</p>\r\n','woman'),(19,'статья про сапоги','<p>сапоги мужские</p>\r\n','man'),(20,'статья про юбки ','<p>юбки женские</p>\r\n','woman'),(21,'статья про трусы сатиновые','<p>трусы сатиновые на подкладке</p>\r\n','man'),(22,'пробная статья в которй дохуя текста','<p>В <a href=\"https://ru.wikipedia.org/wiki/1990_%D0%B3%D0%BE%D0%B4\" title=\"1990 год\">1990&nbsp;году</a> <a href=\"https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D0%BD%D0%B8%D1%81%D1%82%D0%B5%D1%80%D1%81%D1%82%D0%B2%D0%BE_%D0%BE%D0%B1%D0%BE%D1%80%D0%BE%D0%BD%D1%8B_%D0%A1%D0%A1%D0%A1%D0%A0\" title=\"Министерство обороны СССР\">Министерство обороны СССР</a> объявило <a href=\"https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BD%D0%BA%D1%83%D1%80%D1%81\" title=\"Конкурс\">конкурс</a> на новый пистолет, призванный заменить состоящий на вооружении, но не вполне отвечающий современным требованиям пистолет <a href=\"https://ru.wikipedia.org/wiki/%D0%9F%D0%B8%D1%81%D1%82%D0%BE%D0%BB%D0%B5%D1%82_%D0%9C%D0%B0%D0%BA%D0%B0%D1%80%D0%BE%D0%B2%D0%B0\" title=\"Пистолет Макарова\">ПМ</a> (программа НИОКР &laquo;Грач&raquo;). В <a href=\"https://ru.wikipedia.org/wiki/1993_%D0%B3%D0%BE%D0%B4\" title=\"1993 год\">1993 году</a> на этот конкурс был представлен <em>пистолет конструкции Ярыгина</em>. По результатам испытаний, в <a href=\"https://ru.wikipedia.org/wiki/2000_%D0%B3%D0%BE%D0%B4\" title=\"2000 год\">2000 году</a> пистолет (получивший наименование MP-443 &laquo;Грач&raquo;) стал победителем конкурса. В <a href=\"https://ru.wikipedia.org/wiki/2003_%D0%B3%D0%BE%D0%B4\" title=\"2003 год\">2003 году</a> под наименованием &laquo;9-мм пистолет Ярыгина&raquo; (ПЯ) он был принят на вооружение <a class=\"mw-redirect\" href=\"https://ru.wikipedia.org/wiki/%D0%92%D0%BE%D0%BE%D1%80%D1%83%D0%B6%D1%91%D0%BD%D0%BD%D1%8B%D0%B5_%D1%81%D0%B8%D0%BB%D1%8B_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B9%D1%81%D0%BA%D0%BE%D0%B9_%D0%A4%D0%B5%D0%B4%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8\" title=\"Вооружённые силы Российской Федерации\">Вооружённых сил Российской Федерации</a>.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"/ckfinder/userfiles/images/300px-MP-443_Grach_06.jpg\" style=\"height:200px; width:300px\" /></p>\r\n\r\n<p>По состоянию на начало <a href=\"https://ru.wikipedia.org/wiki/2010_%D0%B3%D0%BE%D0%B4\" title=\"2010 год\">2010 года</a>, пистолеты Ярыгина начали поступать на вооружение подразделений ВС России, внутренних войск, специальных подразделений МВД РФ и иных силовых структур.</p>\r\n\r\n<p>В <a href=\"https://ru.wikipedia.org/wiki/2011_%D0%B3%D0%BE%D0%B4\" title=\"2011 год\">2011 году</a> был налажен массовый выпуск ПЯ для армии РФ<sup><a href=\"https://ru.wikipedia.org/wiki/%D0%9F%D0%B8%D1%81%D1%82%D0%BE%D0%BB%D0%B5%D1%82_%D0%AF%D1%80%D1%8B%D0%B3%D0%B8%D0%BD%D0%B0#cite_note-2\">[2]</a></sup>. В <a href=\"https://ru.wikipedia.org/wiki/2012_%D0%B3%D0%BE%D0%B4\" title=\"2012 год\">2012 году</a> пистолет Ярыгина как новое штатное оружие начали осваивать офицеры Западного ВО.<sup><a href=\"https://ru.wikipedia.org/wiki/%D0%9F%D0%B8%D1%81%D1%82%D0%BE%D0%BB%D0%B5%D1%82_%D0%AF%D1%80%D1%8B%D0%B3%D0%B8%D0%BD%D0%B0#cite_note-3\">[3]</a></sup></p>\r\n','man'),(23,'Статья тест','<p>прапрпарпара</p>\r\n','woman');

/*Table structure for table `tovar` */

DROP TABLE IF EXISTS `tovar`;

CREATE TABLE `tovar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name_content` varchar(255) DEFAULT NULL,
  `sex_category` varchar(255) DEFAULT NULL,
  `clothing_category` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `name_magazin` varchar(11) DEFAULT NULL,
  `photo_tovara` varchar(250) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

/*Data for the table `tovar` */

insert  into `tovar`(`id`,`parent_id`,`name_content`,`sex_category`,`clothing_category`,`description`,`name_magazin`,`photo_tovara`) values (18,0,'Брюки','man','odezda','',NULL,NULL),(19,0,'Куртки','man','odezda','',NULL,NULL),(20,0,'Платья','woman','odezda','',NULL,NULL),(23,18,'Брюки в полоску','man','odezda','','11','upload/19.jpg'),(24,20,'платье 1','woman','odezda','','12','upload/01.jpg'),(25,20,'Платье 2','woman','odezda','','12','upload/03.jpg'),(26,20,'Платье 3','woman','odezda','','12','upload/06.jpg'),(27,19,'Куртка мужская','man','odezda','','13','upload/05.jpg'),(28,32,'Куртка женская','woman','odezda','','13','upload/14.jpg'),(29,19,'Куртка мужская 2','man','odezda','','13','upload/07.jpg'),(30,32,'куртка женская 2','woman','odezda','','13','upload/12.jpg'),(31,20,'платье 4','woman','odezda','','12','upload/25.jpg'),(32,0,'Куртки','woman','odezda','',NULL,NULL),(36,0,'Тапки','man','obuv','',NULL,NULL),(37,36,'тапки_1','man','obuv','','14','upload/30.jpg'),(38,36,'тапки_2','man','obuv','','14','upload/31.jpg'),(39,36,'тапки_3','man','obuv','','14','upload/32.jpg'),(41,0,'Сапоги','man','obuv','',NULL,NULL),(42,41,'Сапоги_1','man','obuv','','14','upload/35.jpg'),(43,41,'Сапоги_2','man','obuv','','14','upload/36.jpg'),(44,41,'Сапоги_3','man','obuv','','14','upload/37.jpg'),(45,19,'Рубашка розовая','man','odezda','','13','upload/02.jpg'),(46,18,'Брюки мужские','man','odezda','','11','upload/18.jpg'),(47,18,'Брюки мужские костюмные','man','odezda','','11','upload/21.jpg'),(48,0,'шапки','man','golovnie_ubori','',NULL,NULL),(49,0,'кепки','man','golovnie_ubori','',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `reg_date` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `photo_user` varchar(50) DEFAULT NULL,
  `adm` varchar(5) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`last_name`,`login`,`password`,`mail`,`reg_date`,`country`,`city`,`photo_user`,`adm`) values (85,'oleg','kolmyk','luger','9d3dc7094d3dcb31ffe2960ad891dd04','test@test','Mon, 20 Apr 2015 22:23:18 +0300','ukr','khr','upload/85.jpg','1'),(86,'vasa','vaskin','vasa','9d3dc7094d3dcb31ffe2960ad891dd04','test10@test10','Mon, 20 Apr 2015 22:27:25 +0300','ukr','khr','upload/86.jpg',NULL),(87,'peta','petrov','peta','9d3dc7094d3dcb31ffe2960ad891dd04','test3@test3','Wed, 22 Apr 2015 23:08:05 +0300','ukr','khr','upload/87.jpg',NULL),(88,'test','test','test','9d3dc7094d3dcb31ffe2960ad891dd04','test3@test3','Thu, 23 Apr 2015 11:00:56 +0300','test','test','upload/88.jpg',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
